package main

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"log"
	"golang.org/x/net/context"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/vision/v1"
	"mime/multipart"
	"bytes"
	"io"
	"database/sql"
	_ "github.com/lib/pq"
)

const (
	DB_USER     = "postgres"
	DB_PASSWORD = "programming"
	DB_NAME     = "postgres"
)

var db *sql.DB

func main() {
	var err error

	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable port=9343",
		DB_USER, DB_PASSWORD, DB_NAME)
	db, err = sql.Open("postgres", dbinfo)

	if err != nil {
		panic(err)
	}

	err = db.Ping()

	if err != nil {
		panic(err)
	}

	defer db.Close()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Request!!!")
		fmt.Fprintf(w, "Hello from Docker")
	})

	http.HandleFunc("/upload", upload)

	fmt.Println("Listening on :9345")
	log.Fatal(http.ListenAndServe(":9345", nil))
}

func upload(w http.ResponseWriter, r *http.Request) {
	fmt.Println("method:", r.Method)
	if r.Method == "POST" {
		fmt.Println("Uploading file")
		r.ParseMultipartForm(32 << 20)

		formdata := r.MultipartForm
		files := formdata.File["multiplefiles"] // grab the filenames

		fmt.Println("Uploading file")

		for i, _ := range files { // loop through the files one by one
			imageFile, err := files[i].Open()
			imageName:=files[i].Filename

			defer imageFile.Close()

			if err != nil {
				fmt.Fprintln(w, err)
				return
			}

			bytesImage, err := toBytes(imageFile)

			if err != nil {
				panic(err)
			}

			go run(bytesImage,imageName)
		}
	}
}

func run(file []byte, filename string) {
	ctx := context.Background()

	client, err := google.DefaultClient(ctx, vision.CloudPlatformScope)
	if err != nil {
		panic(err)
		fmt.Println("DefaultClient occured :CCCC")
		return
	}

	service, err := vision.New(client)

	if err != nil {
		fmt.Println("vision occured :CCCC")
		return
	}

	req := &vision.AnnotateImageRequest{
		// Apply image which is encoded by base64
		Image: &vision.Image{
			Content: base64.StdEncoding.EncodeToString(file),
		},
		// Apply features to indicate what type of image detection
		Features: []*vision.Feature{
			{
				Type:       "LABEL_DETECTION",
				MaxResults: 5,
			},
		},
	}

	batch := &vision.BatchAnnotateImagesRequest{
		Requests: []*vision.AnnotateImageRequest{req},
	}

	res, err := service.Images.Annotate(batch).Do()
	if err != nil {
		panic(err)
		fmt.Println("service occured :CCCC")
		return
	}

	if annotations := res.Responses[0].LabelAnnotations; len(annotations) > 0 {
		if len(annotations) > 0{
			addToDatabase(filename, annotations[0].Description, annotations[0].Score)
		}

		fmt.Println("Responses end occured :)")
		return
	}

	fmt.Printf("Not found label")
}

func toBytes(file multipart.File) ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	if _, err := io.Copy(buf, file); err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

func addToDatabase(filaname string, prediction string, probability float64) {
	db.QueryRow("INSERT INTO test_table VALUES($1,$2,$3) ON CONFLICT (image) DO UPDATE SET label = $2, probability = $3;", filaname, prediction, probability)
}
